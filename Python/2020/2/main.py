from _io    import TextIOWrapper
from re     import compile, Pattern, Match
from typing import List, NamedTuple

class Password(NamedTuple):
    first:    int
    second:   int
    letter:   str
    password: str

def parse_input(input_path: str) -> List[Password]:
    passwords: List[Password] = []

    regex: Pattern = compile(r'(\d+)\-(\d+)\s+(\w):\s+(\w+)')

    file: TextIOWrapper
    line: str

    with open(input_path) as file:
        for line in file:
            line = line.strip()

            search_result: Match = regex.search(line)

            if search_result is not None:
                passwords.append(Password(int(search_result.group(1)), 
                                          int(search_result.group(2)), 
                                          search_result.group(3), 
                                          search_result.group(4)))

    return passwords

def part_one(input_path: str) -> None:
    passwords:            List[Password] = parse_input(input_path)
    valid_password_count: int            = 0

    password: Password

    for password in passwords:
        letter_count: int = len(password.password) - len(password.password.replace(password.letter, ''))

        if letter_count in range(password.first, password.second + 1):
            valid_password_count += 1

    print("PART 1:")
    print("Number of valid passwords: " + str(valid_password_count))
    print()

def part_two(input_path: str) -> None:
    passwords:            List[Password] = parse_input(input_path)
    valid_password_count: int            = 0

    password: Password

    for password in passwords:
        index_count: int = sum([1 if password.password[i] == password.letter else 0 
                                for i in [password.first - 1, password.second - 1]])

        if index_count == 1:
            valid_password_count += 1

    print("PART 2:")
    print("Number of valid passwords: " + str(valid_password_count))
    print()

if __name__ == "__main__":
    label:      str
    input_path: str

    for label, input_path in [("EXAMPLE", "example.txt"), ("PUZZLE", "input.txt")]:
        print(label)
        print('=' * len(label))
        print()

        part_one(input_path)
        part_two(input_path)
