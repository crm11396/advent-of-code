from _io    import TextIOWrapper
from typing import List

def parse_input(input_path: str) -> List[int]:
    entries: List[int] = []

    file: TextIOWrapper
    line: str

    with open(input_path) as file:
        for line in file:
            line = line.strip()

            entries.append(int(line))

    return entries

def part_one(input_path: str) -> None:
    entries: List[str] = parse_input(input_path)
    found:   bool      = False
    first:   int       = 0
    second:  int       = 0

    i: int
    j: int

    for i in range(len(entries)):
        for j in range(i + 1, len(entries)):
            if entries[i] + entries[j] == 2020:
                first, second = entries[i], entries[j]
                found = True
                break
        if found:
            break

    print("PART 1:")
    print("Product of two entries that sum to 2020: " + str(first * second))
    print()

def part_two(input_path: str) -> None:
    entries: List[str] = parse_input(input_path)
    found:   bool      = False
    first:   int       = 0
    second:  int       = 0
    third:   int       = 0

    i: int
    j: int
    k: int

    for i in range(len(entries)):
        for j in range(i + 1, len(entries)):
            for k in range(j + 1, len(entries)):
                if entries[i] + entries[j] + entries[k] == 2020:
                    first, second, third = entries[i], entries[j], entries[k]
                    found = True
                    break
            if found:
                break
        if found:
            break
    
    print("PART 2:")
    print("Product of three entries that sum to 2020: " + str(first * second * third))
    print()

if __name__ == "__main__":
    label:      str
    input_path: str

    for label, input_path in [("EXAMPLE", "example.txt"), ("PUZZLE", "input.txt")]:
        print(label)
        print('=' * len(label))
        print()

        part_one(input_path)
        part_two(input_path)
