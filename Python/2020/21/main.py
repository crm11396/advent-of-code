from _io    import TextIOWrapper
from typing import Dict, List, Set, Tuple

StrSet  = Set[str]
Input   = Tuple[Dict[str, List[StrSet]], StrSet, List[StrSet]]

def parse_input(input_path: str) -> Input:
    allergens:       Dict[str, List[StrSet]] = {}
    all_ingredients: StrSet                  = set()
    all_foods:       List[StrSet]            = []

    file: TextIOWrapper
    line: str

    with open(input_path) as file:
        for line in file:
            line = line.strip()

            food_parts:      List[str] = line.split(' (')
            ingredients:     StrSet    = set(food_parts[0].split())
            known_allergens: StrSet    = set(food_parts[1].replace('contains ', '').replace(')', '').split(', '))

            all_ingredients = all_ingredients.union(ingredients)
            all_foods.append(ingredients)

            allergen: str

            for allergen in known_allergens:
                if allergen in allergens:
                    allergens[allergen].append(ingredients)
                else:
                    allergens[allergen] = [ingredients]
    
    return allergens, all_ingredients, all_foods

def find_allergens(input_path: str) -> Tuple[List[StrSet], StrSet, Dict[str, StrSet]]:
    allergens:       Dict[str, List[StrSet]]
    all_ingredients: StrSet
    all_foods:       List[StrSet]

    allergen:   str
    others:     str
    ingredient: str

    allergens, all_ingredients, all_foods = parse_input(input_path)

    possible_allergens: Dict[str, StrSet] = {allergen: set.intersection(*allergens[allergen]) for allergen in allergens}

    while any([len(possible_allergens[allergen]) > 1 for allergen in possible_allergens]):
        for allergen in possible_allergens:
            if len(possible_allergens[allergen]) == 1:
                ingredient = list(possible_allergens[allergen])[0]

                for others in possible_allergens:
                    if allergen == others:
                        continue

                    if ingredient in possible_allergens[others]:
                        possible_allergens[others].remove(ingredient)

    for ingredient in [list(possible_allergens[allergen])[0] for allergen in possible_allergens]:
        all_ingredients.remove(ingredient)

    return all_foods, all_ingredients, possible_allergens

def part_one(all_foods: List[StrSet], all_ingredients: StrSet) -> None:
    ingredient_counts: int = 0

    ingredient: str
    food:       StrSet

    for ingredient in all_ingredients:
        ingredient_counts += sum([1 if ingredient in food else 0 for food in all_foods])

    print("PART 1:")
    print("Number of times any ingredient occurs: " + str(ingredient_counts))
    print()

def part_two(possible_allergens: Dict[str, List[StrSet]]) -> None:
    allergen: str
    alrg:     str

    dangerous_ingredient_list: str = \
        ','.join([list(possible_allergens[allergen])[0] for allergen in sorted([alrg for alrg in possible_allergens])])

    print("PART 2:")
    print("Canonical dangerous ingredient list: " + dangerous_ingredient_list)
    print()

if __name__ == "__main__":
    label:      str
    input_path: str

    all_foods:          List[StrSet]
    all_ingredients:    StrSet
    possible_allergens: Dict[str, List[StrSet]]

    for label, input_path in [("EXAMPLE", 'example.txt'), ("PUZZLE", 'input.txt')]:
        print(label)
        print('=' * len(label))
        print()

        all_foods, all_ingredients, possible_allergens = find_allergens(input_path)

        part_one(all_foods, all_ingredients)
        part_two(possible_allergens)
