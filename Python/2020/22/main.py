from _io         import TextIOWrapper
from collections import deque
from typing      import Set, Tuple

IntDeque = deque
RoundSet = Set[Tuple[Tuple[int], Tuple[int]]]

def parse_input(input_path: str) -> Tuple[IntDeque, IntDeque, IntDeque, IntDeque]:
    part_1_player_1_hand: IntDeque = deque([])
    part_1_player_2_hand: IntDeque = deque([])
    part_2_player_1_hand: IntDeque = deque([])
    part_2_player_2_hand: IntDeque = deque([])

    phase: int = 0

    file: TextIOWrapper
    line: str

    with open(input_path) as file:
        for line in file:
            line = line.strip()

            if len(line) == 0:
                continue

            if "Player 1:" in line:
                phase = 1
                continue

            if "Player 2:" in line:
                phase = 2
                continue

            value: int = int(line)

            if phase == 1:
                part_1_player_1_hand.appendleft(value)
                part_2_player_1_hand.appendleft(value)

            elif phase == 2:
                part_1_player_2_hand.appendleft(value)
                part_2_player_2_hand.appendleft(value)

    return part_1_player_1_hand, part_1_player_2_hand, part_2_player_1_hand, part_2_player_2_hand

def get_winner_of_game(player_1_hand: IntDeque, player_2_hand: IntDeque, do_recursion: bool) -> IntDeque:
    previous_rounds: RoundSet = set()

    while len(player_1_hand) > 0 and len(player_2_hand) > 0:
        execute_round(player_1_hand, player_2_hand, previous_rounds, do_recursion)        
        
    winner: IntDeque = player_1_hand if len(player_1_hand) > len(player_2_hand) else player_2_hand

    return winner

def execute_round(player_1_hand: IntDeque, player_2_hand: IntDeque, previous_rounds: RoundSet, do_recursion: bool) -> None:
    if do_recursion and (tuple(player_1_hand), tuple(player_2_hand)) in previous_rounds:
        player_2_hand.clear()

    else:
        previous_rounds.add((tuple(player_1_hand), tuple(player_2_hand)))

        player_1_card: int = player_1_hand.pop()
        player_2_card: int = player_2_hand.pop()

        if do_recursion and len(player_1_hand) >= player_1_card and len(player_2_hand) >= player_2_card:
            player_1_sub_hand: IntDeque = deque(list(player_1_hand)[-player_1_card:])
            player_2_sub_hand: IntDeque = deque(list(player_2_hand)[-player_2_card:])

            give_winner_cards(player_1_sub_hand is get_winner_of_game(player_1_sub_hand, player_2_sub_hand, do_recursion),
                              player_1_hand, player_1_card,
                              player_2_hand, player_2_card)

        else:
            give_winner_cards(player_1_card > player_2_card,
                              player_1_hand, player_1_card,
                              player_2_hand, player_2_card)

def give_winner_cards(player_1_win_condition: bool, player_1_hand: IntDeque, player_1_card: int, player_2_hand: IntDeque, player_2_card: int) -> None:
    if player_1_win_condition:
        player_1_hand.appendleft(player_1_card)
        player_1_hand.appendleft(player_2_card)

    else:
        player_2_hand.appendleft(player_2_card)
        player_2_hand.appendleft(player_1_card)

def calculate_hand_score(hand: IntDeque) -> int:
    score: int = 0

    i: int

    for i in range(len(hand), 0, -1):
        score += i * hand.pop()

    return score

def part_one(player_1_hand: IntDeque, player_2_hand: IntDeque) -> None:
    winner: IntDeque = get_winner_of_game(player_1_hand, player_2_hand, False)

    print("PART 1:")
    print("Score of winning hand: " + str(calculate_hand_score(winner)))
    print()

def part_two(player_1_hand: IntDeque, player_2_hand: IntDeque) -> None:
    winner: IntDeque = get_winner_of_game(player_1_hand, player_2_hand, True)

    print("PART 2:")
    print("Score of winning hand: " + str(calculate_hand_score(winner)))
    print()

if __name__ == "__main__":
    label:      str
    input_path: str

    part_1_player_1_hand: IntDeque
    part_1_player_2_hand: IntDeque
    part_2_player_1_hand: IntDeque
    part_2_player_2_hand: IntDeque

    for label, input_path in [("EXAMPLE", "example.txt"), ("PUZZLE", "input.txt")]:
        print(label)
        print('=' * len(label))
        print()

        part_1_player_1_hand, part_1_player_2_hand, part_2_player_1_hand, part_2_player_2_hand = parse_input(input_path)

        part_one(part_1_player_1_hand, part_1_player_2_hand)
        part_two(part_2_player_1_hand, part_2_player_2_hand)
