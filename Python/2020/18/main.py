import re

from _io    import TextIOWrapper
from typing import List

def parse_input(input_path: str) -> List[str]:
    expressions: List[str] = []

    file:        TextIOWrapper
    line:        str

    with open(input_path) as file:
        for line in file:
            line = line.strip()

            if len(line) == 0:
                continue

            expressions.append(line)

    return expressions

def evaluate_expression(expression: str, evaluate_addition_first: bool) -> int:
    while '(' in expression:
        matches: List[str] = re.findall(r"\([^\(]*?\)", expression)
        match:   str

        for match in matches:
            expression = expression.replace(match, evaluate_simple_expression(match, evaluate_addition_first))

    return int(evaluate_simple_expression(expression, evaluate_addition_first))

def evaluate_simple_expression(expression: str, evaluate_addition_first: bool) -> str:
    expression_parts: List[str]

    expression = expression.replace("(", "").replace(")", "")

    if evaluate_addition_first:
        expression_parts = evaluate_addition(expression)
    else:
        expression_parts = expression.split()

    result:   int = int(expression_parts[0])
    operator: str = ''

    element:  str

    for element in expression_parts[1:]:
        if element == '+' or element == '*':
            operator = element

        else:
            num: int = int(element)

            if operator == '+':
                result += num
            else:
                result *= num

    return str(result)

def evaluate_addition(expression: str) -> List[str]:
    expression_parts: List[str] = expression.split()
    addition_indices: List[int] = sorted([i for i, element in enumerate(expression_parts) if element == '+'], reverse = True)

    i: int

    for i in addition_indices:
        left:  int = int(expression_parts[i - 1])
        right: int = int(expression_parts[i + 1])

        result: int = left + right

        del expression_parts[i - 1 : i + 2]

        expression_parts.insert(i - 1, str(result))

    return expression_parts

def execute(parsed_input: List[str], evaluate_addition_first: bool) -> None:
    result: int = 0

    expression: str

    for expression in parsed_input:
        result += evaluate_expression(expression, evaluate_addition_first)

    print('Sum of evaluated expressions: ' + str(result))

def part_one(parsed_input: List[str]) -> None:
    print('PART 1:')
    execute(parsed_input, False)
    print()

def part_two(parsed_input: List[str]) -> None:
    print('PART 2:')
    execute(parsed_input, True)
    print()

if __name__ == "__main__":
    parsed_input: List[str] = parse_input('input.txt')

    part_one(parsed_input)
    part_two(parsed_input)
