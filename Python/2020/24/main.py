from _io    import TextIOWrapper
from typing import Dict, List, Set, Tuple

def parse_input(input_path: str) -> Set[Tuple[int, int]]:
    tile_paths: List[List[str]] = []
    tile_path:  List[str]       = []
    
    direction: str = ''

    file: TextIOWrapper
    line: str
    char: str

    with open(input_path) as file:
        for line in file:
            line = line.strip()

            for char in line:
                direction += char

                if direction == 's' or direction == 'n':
                    continue

                tile_path.append(direction)
                direction = ''

            tile_paths.append(tile_path)
            tile_path = []

    flipped: Set[Tuple[int, int]] = set()

    for tile_path in tile_paths:
        x, y = navigate(tile_path)

        if (x, y) in flipped:
            flipped.remove((x, y))
        else:
            flipped.add((x, y))

    return flipped

def navigate(sequence: List[str]) -> Tuple[int, int]:
    x: int = 0
    y: int = 0

    step: str

    for step in sequence:
        if   step == 'e':  (x, y) = (x + 1, y)
        elif step == 'w':  (x, y) = (x - 1, y)
        elif step == 'ne': (x, y) = (x,     y - 1) if y % 2 == 0 else (x + 1, y - 1)
        elif step == 'nw': (x, y) = (x - 1, y - 1) if y % 2 == 0 else (x,     y - 1)
        elif step == 'se': (x, y) = (x,     y + 1) if y % 2 == 0 else (x + 1, y + 1)
        elif step == 'sw': (x, y) = (x - 1, y + 1) if y % 2 == 0 else (x,     y + 1)

    return x, y

def get_neighbors(x: int, y: int) -> List[Tuple[int, int]]:
    return [(x - 1, y - 1),
            (x,     y - 1),
            (x + 1, y),
            (x,     y + 1),
            (x - 1, y + 1),
            (x - 1, y)]       \
           if y % 2 == 0 else \
           [(x,     y - 1),
            (x + 1, y - 1),
            (x + 1, y),
            (x + 1, y + 1),
            (x,     y + 1),
            (x - 1, y)]

def flip_tiles(flipped: Set[Tuple[int, int]]) -> None:
    to_flip:           Set[Tuple[int, int]]       = set()
    white_tile_counts: Dict[Tuple[int, int], int] = {}

    tile:     Tuple[int, int]
    neighbor: Tuple[int, int]

    for tile in flipped:

        neighbors:       List[Tuple[int, int]] = get_neighbors(*tile)
        black_neighbors: int                   = sum([1 if neighbor in flipped else 0 for neighbor in neighbors])

        if black_neighbors == 0 or black_neighbors > 2:
            to_flip.add(tile)

        for neighbor in neighbors:
            if neighbor not in flipped:
                if neighbor in white_tile_counts:
                    white_tile_counts[neighbor] += 1
                else:
                    white_tile_counts[neighbor] = 1

    for tile in white_tile_counts:
        if white_tile_counts[tile] == 2:
            to_flip.add(tile)

    for tile in to_flip:
        if tile in flipped:
            flipped.remove(tile)
        else:
            flipped.add(tile)

def part_one(flipped: Set[Tuple[int, int]]) -> None:
    print("PART 1:")
    print("Number of flipped tiles: " + str(len(flipped)))
    print()

def part_two(flipped: Set[Tuple[int, int]]) -> None:
    for _ in range(100):
        flip_tiles(flipped)

    print("PART 2:")
    print("Number of flipped tiles: " + str(len(flipped)))
    print()

if __name__ == "__main__":
    label:      str
    input_path: str

    for label, input_path in [("EXAMPLE", "example.txt"), ("PUZZLE", "input.txt")]:
        print(label)
        print('=' * len(label))
        print()

        initial_flipped: Set[Tuple[int, int]] = parse_input(input_path)

        part_one(initial_flipped)
        part_two(initial_flipped)
